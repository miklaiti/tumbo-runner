﻿using UnityEngine;
using System.Collections;

public class backgroundScroll : MonoBehaviour
{

    public Renderer background;

    public float backgroundSpeed;
 

    public float offset;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float backgroundOffset = offset * backgroundSpeed;
    

        background.material.mainTextureOffset = new Vector2(backgroundOffset, 0);
        

    }

  
}
