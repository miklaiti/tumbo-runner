﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {
    public Transform platformGenerator;
    private Vector3 platformStartPoint;



    public PlayerController thePlayer;
    private Vector3 playerStartPoint;

    private ScoreManager scoreManager;

    private PlatformDestroyer[] platformList;
    // Use this for initialization
    void Start () {
        scoreManager = ScoreManager.FindObjectOfType<ScoreManager>();
        
        platformStartPoint = platformGenerator.position;
        playerStartPoint = thePlayer.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void restartGame() {

        StartCoroutine("RestartGameCo");
    }

 

    public IEnumerator RestartGameCo() {
        scoreManager.scoreIncreasing = false;
        
        thePlayer.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.5f);

        platformList = FindObjectsOfType<PlatformDestroyer>();
        for (int i = 0; i < platformList.Length; i++) {
           platformList[i].gameObject.SetActive(false);
        }
        thePlayer.transform.position = playerStartPoint;
        platformGenerator.position = platformStartPoint;
        thePlayer.gameObject.SetActive(true);

 
        scoreManager.scoreCount = 0;
        scoreManager.scoreIncreasing = true;
    }
}
