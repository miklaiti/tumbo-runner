﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
    public float moveSpeed;
    public float Speedmultiplier;
    private float moveSpeedStore;

    public float speedIncreaseMilestone;
    private float speedIncreaseMileStoneStore;
    public float jumpForce;
    public bool grounded;
    public float jumpTime;
    private float speedMileStoneCount;
    private float speedMilestoneCountStore;
    public Transform groundCheck;
    public float groundCheckRadius;
    public LayerMask whatIsGround;
    private float jumpTimeCounter;
    private Rigidbody2D myRigidbody;
    private Animator playerAnimator;

    public backgroundScroll parallax;

    public GameManager theGameManager;



    // Use this for initialization
    void Start () {
        
        myRigidbody = GetComponent<Rigidbody2D>();
        //myCollider = GetComponent<Collider2D>();
        playerAnimator = GetComponent<Animator>();
        jumpTimeCounter = jumpTime;
        speedMileStoneCount = speedIncreaseMilestone;

        moveSpeedStore = moveSpeed;
        speedMilestoneCountStore = speedMileStoneCount;
        speedIncreaseMileStoneStore = speedIncreaseMilestone;
    }

    // Update is called once per frame
    void Update() {
        
            //grounded = Physics2D.IsTouchingLayers(myCollider, whatIsGround);

            grounded = Physics2D.OverlapCircle(groundCheck.position,groundCheckRadius, whatIsGround);
        if (transform.position.x > speedMileStoneCount) {
            speedMileStoneCount += speedIncreaseMilestone;
            speedIncreaseMilestone += speedMileStoneCount * Speedmultiplier;
            moveSpeed = moveSpeed * Speedmultiplier;
        }

        myRigidbody.velocity = new Vector2(moveSpeed, myRigidbody.velocity.y);


        if (grounded)
        {
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
            {
                myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, jumpForce);
            }
        }

        if (Input.GetMouseButton(0))
        {
            if (jumpTimeCounter > 0)
            {
                myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, jumpForce);
                jumpTimeCounter -= Time.deltaTime;
            }
        }


        if(Input.GetMouseButtonUp(0)){
            jumpTimeCounter = 0;
        }

        if (grounded) {
            jumpTimeCounter = jumpTime;
        }

        playerAnimator.SetBool("grounded", grounded);   
        playerAnimator.SetFloat("speed", myRigidbody.velocity.x);
      

      
	}

    void OnTriggerEnter2D(Collider2D col) {
        
          if (col.gameObject.tag == "KILLBOX") {
            
            theGameManager.restartGame();
            moveSpeed = moveSpeedStore;
            speedMileStoneCount = speedMilestoneCountStore;
            speedIncreaseMilestone = speedIncreaseMileStoneStore;
        }


    }

    void FixedUpdate()
    {

        parallax.offset = transform.position.x;
    }
}
