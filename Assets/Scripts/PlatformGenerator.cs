﻿using UnityEngine;
using System.Collections;

public class PlatformGenerator : MonoBehaviour {

    //public GameObject thePlatform;
    public Transform generationPoint;
    public float distanceBetween;
    public float distanceBetweenMin;
    public float distanceBetweenMax;
    public ObjectPooler [] theObjectPools;

    private float platformWidth;
    private float maxHeight;
    public Transform maxHeightPoint;
    private float minHeight;
    public float maxHeightChange;
    private float heightChange;

    private float [] platformWidths;
    //public GameObject[] thePlatforms;
    private int platformSelector;
	// Use this for initialization
	void Start () {
        //platformWidth = thePlatform.GetComponent<BoxCollider2D>().size.x;

        platformWidths = new float[theObjectPools.Length];

        for (int i = 0; i < theObjectPools.Length; i++) {
            platformWidths[i] = theObjectPools[i].pooledObject.GetComponent<BoxCollider2D>().size.x;                
        }
        minHeight = transform.position.y;
        maxHeight = maxHeightPoint.position.y;
    }
	
	// Update is called once per frame
	void Update () {


        if (transform.position.x < generationPoint.position.x) {

            heightChange = transform.position.y + Random.Range(maxHeightChange,-maxHeightChange);
            if (heightChange > maxHeight ) {
                heightChange = maxHeight;
            }

            else if (heightChange < minHeight)
            {
                heightChange = minHeight;
            }
            distanceBetween = Random.Range(distanceBetweenMin, distanceBetweenMax);
            platformSelector = Random.Range(0, theObjectPools.Length);
            transform.position = new Vector3(transform.position.x + (platformWidths[platformSelector]/2) + distanceBetween , heightChange, transform.position.z);
            //Instantiate(thePlatforms[platformSelector], transform.position,transform.rotation);

           GameObject newPlatform = theObjectPools[platformSelector].GetPooledObject();
            
            newPlatform.transform.position = transform.position;
            newPlatform.transform.rotation = transform.rotation;
            newPlatform.SetActive(true);
            transform.position = new Vector3(transform.position.x + (platformWidths[platformSelector] / 2), transform.position.y, transform.position.z);

        }
    }
}
