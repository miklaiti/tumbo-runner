﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

    public Text scoreText;
    public Text HiscoreText;
    public float pointsPerSecond;
    public float scoreCount;
    public float hiScoreCount;
    public bool scoreIncreasing;
    // Use this for initialization
    void Start () {
        if (PlayerPrefs.GetFloat("HighScoreCount", hiScoreCount) > 0) {
            hiScoreCount = PlayerPrefs.GetFloat("HighScoreCount",hiScoreCount);
        }
    }

    // Update is called once per frame
    void FixedUpdate() {
        if (scoreIncreasing) { 
            scoreCount += pointsPerSecond * Time.deltaTime;
        }

        if (scoreCount > hiScoreCount) {
            hiScoreCount = scoreCount;
            PlayerPrefs.SetFloat("HighScoreCount", hiScoreCount);
        }

        scoreText.text = "Score : " + Mathf.Round(scoreCount);
        HiscoreText.text = "High Score : " + Mathf.Round(hiScoreCount);
    }
}
